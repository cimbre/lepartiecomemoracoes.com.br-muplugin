<?php
/*
Plugin Name: Le Partie Comemorações - mu-plugin
Plugin URI: https://lepartiecomemoracoes.com.br
Description: Customizações para o site lepartiecomemoracoes.com.br
Author: Estúdio Cimbre Design
Version: 1.0.0.1
Author URI: http://www.estudiocimbre.com.br/
Text Domain: lepartiecomemoracoes-wp-muplugin
*/

// Plugin constants
define('ADDRESS_LOCATION', 'Le+Partie+comemorações');

/**
 * Load Translations
 */
add_action('plugins_loaded', 'lepartiecomemoracoes_load_textdomain');
function lepartiecomemoracoes_load_textdomain()     
{
    load_muplugin_textdomain('lepartiecomemoracoes-wp-muplugin', basename(dirname(__FILE__)) . '/languages');
}

/**
 * Page Custom Fields
 */
function Cmb_LepartieComemoracoes_page() 
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_lepartie_';
    
    /********************************************************************
     * Home section
     *******************************************************************/
    $section = 'home_';
    $cmb_home = new_cmb2_box(
        array(
            'id'            => 'home_id',
            'title'         => __('Home', 'lepartiecomemoracoes-wp-muplugin'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'front-page', 'value' => '' ), // function should return a bool value
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    //Home Background Image
    $cmb_home->add_field(
        array(
        'name'        => __('Background Image', 'lepartiecomemoracoes-wp-muplugin'),
        'description' => '',
        'id'          => $prefix.$section.'bg-image',
        'type'        => 'file',
        // Optional:
        'options' => array(
                'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
                'add_upload_file_text' => __('Add Image', 'lepartiecomemoracoes-wp-muplugin'), // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
                //'type' => 'image/jpeg', // Make library only display PDFs.
                // Or only allow gif, jpg, or png images
                'type' => array(
                // 	'image/gif',
                        'image/jpeg',
                        'image/png',
               ),
        ),
        'preview_size' => array(320, 180) //'large', // Image size to use when previewing in the admin.
        )
    );

    /********************************************************************
     * About section
     *******************************************************************/
    $section = 'about_';
    $cmb_about = new_cmb2_box(
        array(
            'id'            => 'about_id',
            'title'         => __('About', 'lepartiecomemoracoes-wp-muplugin'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'front-page', 'value' => '' ), // function should return a bool value
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    //About Background Image
    $cmb_about->add_field(
        array(
        'name'        => __('Background Image', 'lepartiecomemoracoes-wp-muplugin'),
        'description' => '',
        'id'          => $prefix.$section.'bg-image',
        'type'        => 'file',
        // Optional:
        'options' => array(
                'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
                'add_upload_file_text' => __('Add Image', 'lepartiecomemoracoes-wp-muplugin'), // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
                //'type' => 'image/jpeg', // Make library only display PDFs.
                // Or only allow gif, jpg, or png images
                'type' => array(
                // 	'image/gif',
                        'image/jpeg',
                        'image/png',
               ),
        ),
        'preview_size' => array(320, 180) //'large', // Image size to use when previewing in the admin.
        )
    );

    //Description
    $cmb_about->add_field(
        array(
        'name' => __('Description', 'lepartiecomemoracoes-wp-muplugin'),
        'description' => '',
        'id'   => $prefix.$section.'description',
        'type' => 'textarea_small',
        ) 
    );

    //Featured image
    $cmb_about->add_field(
        array(
        'name'        => __('Featured Image', 'lepartiecomemoracoes-wp-muplugin'),
        'description' => '',
        'id'          => $prefix.$section.'featured-image',
        'type'        => 'file',
        // Optional:
        'options' => array(
                'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
                'add_upload_file_text' => __('Add Image', 'lepartiecomemoracoes-wp-muplugin'), // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
                //'type' => 'image/jpeg', // Make library only display PDFs.
                // Or only allow gif, jpg, or png images
                'type' => array(
                // 	'image/gif',
                        'image/jpeg',
                        'image/png',
               ),
        ),
        'preview_size' => array(320, 180) //'large', // Image size to use when previewing in the admin.
        )
    );

    //Testimonials Title
    $cmb_about->add_field( 
        array(
        'name' => __('Testimonials', 'lepartiecomemoracoes-wp-muplugin'),
        'desc' => '',
        'type' => 'title',
        'id'   => $prefix.$section.'testimonials_title',
        )
    );

    //Testimonials Show
    $cmb_about->add_field( 
        array(
        'name' => __('Show Testimonials', 'lepartiecomemoracoes-wp-muplugin'),
        'desc' => __('Show Testimonials section on page', 'lepartiecomemoracoes-wp-muplugin'),
        'type' => 'checkbox',
        'id'   => $prefix.$section.'testimonials_show',
        )
    );

    //Testimonials Group
    $testimonials_id = $cmb_about->add_field( 
        array(
        'id'          => $prefix.$section.'testimonials',
        'type'        => 'group',
        'description' => '', 
        // 'repeatable'  => false, // use false if you want non-repeatable group
        'options'     => array(
            'group_title'   => __('Testimonial {#}', 'lepartiecomemoracoes-wp-muplugin'), // since version 1.1.4, {#} gets replaced by row number
            'add_button'    => __('Add Another Entry', 'lepartiecomemoracoes-wp-muplugin'),
            'remove_button' => __('Remove Entry', 'lepartiecomemoracoes-wp-muplugin'),
            'sortable'      => true, // beta
            // 'closed'     => true, // true to have the groups closed by default
        ),
        )
    );

    //Testimonial profile picture
    $cmb_about->add_group_field(
        $testimonials_id, array(
        'name'    => __('Profile Picture', 'lepartiecomemoracoes-wp-muplugin'),
        'desc'    => '',
        'id'      => 'profile-picture',
        'type'    => 'file',
        // Optional:
        'options' => array(
                'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
                'add_upload_file_text' => __('Add Image', 'lepartiecomemoracoes-wp-muplugin'), // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
                //'type' => 'image/jpeg', // Make library only display PDFs.
                // Or only allow gif, jpg, or png images
                'type' => array(
                // 	'image/gif',
                        'image/jpeg',
                        'image/png',
            ),
        ),
        'preview_size' => array(180, 180) //'large', // Image size to use when previewing in the admin.
        )
    );

    //Testimonial Profile
    $cmb_about->add_group_field( 
        $testimonials_id, array(
        'name' => __('Profile', 'lepartiecomemoracoes-wp-muplugin'),
        'id'   => 'profile',
        'type' => 'text',
        ) 
    );

    //Testimonial Description
    $cmb_about->add_group_field( 
        $testimonials_id, array(
        'name' => __('Description', 'lepartiecomemoracoes-wp-muplugin'),
        'id'   => 'description',
        'description' => '',
        'type' => 'textarea_small',
        ) 
    );
    
    /********************************************************************
     * Structure section
     *******************************************************************/
    $section = 'structure_';
    $cmb_structure = new_cmb2_box(
        array(
            'id'            => 'structure_id',
            'title'         => __('Structure', 'lepartiecomemoracoes-wp-muplugin'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'front-page', 'value' => '' ), // function should return a bool value
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );
    
    //Structure Background image
    $cmb_structure->add_field(
        array(
            'name'    => __('Background Image', 'lepartiecomemoracoes-wp-muplugin'),
            'desc'    => '',
            'id'      => $prefix.$section.'bg-image',
            'type'    => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'lepartiecomemoracoes-wp-muplugin'), // Change upload button text. Default: "Add or Upload File"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                ),
            ),
            'preview_size' => array(320, 180) //'large', // Image size to use when previewing in the admin.
        )
    );

    //Structure Description
    $cmb_structure->add_field( 
        array(
            'name' => __('Description', 'lepartiecomemoracoes-wp-muplugin'),
            'id'   => $prefix.$section.'description',
            'description' => '',
            'type' => 'textarea_small',
        ) 
    );
    
    //Structure Gallery
    $cmb_structure->add_field(
        array(
            'name' => __('Featured Gallery', 'lepartiecomemoracoes-wp-muplugin'),
            'id'   => $prefix.$section.'gallery',
            'type' => 'file_list',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text' => array(
                'add_upload_files_text' => __('Add Images', 'lepartiecomemoracoes-wp-muplugin'), // default: "Add or Upload Files"
                'remove_image_text' => __('Remove Image', 'lepartiecomemoracoes-wp-muplugin'), // default: "Remove Image"
                'file_text' => __('File', 'lepartiecomemoracoes-wp-muplugin'), // default: "File:"
                'file_download_text' => __('Download', 'lepartiecomemoracoes-wp-muplugin'), // default: "Download"
                'remove_text' => __('Remove', 'lepartiecomemoracoes-wp-muplugin'),
                'use_text' => __('Remove', 'lepartiecomemoracoes-wp-muplugin'), // default: "Remove"
                'upload_file'  => __('Use this file', 'lepartiecomemoracoes-wp-muplugin'),
                'upload_files' => __('Use these files', 'lepartiecomemoracoes-wp-muplugin'),
                'remove_image' => __('Remove Image', 'lepartiecomemoracoes-wp-muplugin'),
                'remove_file'  => __('Remove', 'lepartiecomemoracoes-wp-muplugin'),
                'file'         => __('File:', 'lepartiecomemoracoes-wp-muplugin'),
                'download'     => __('Download', 'lepartiecomemoracoes-wp-muplugin'),
                'check_toggle' => __('Select / Deselect All', 'lepartiecomemoracoes-wp-muplugin'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
            ),
            'preview_size' => array(160, 90) //'large', // Image size to use when previewing in the admin.
        )
    );
    
    //Gallery Group Title
    $cmb_structure->add_field( 
        array(
        'name' => __('Gallery', 'lepartiecomemoracoes-wp-muplugin'),
        'desc' => '',
        'type' => 'title',
        'id'   => $prefix.$section.'gallery_group_title',
        )
    );

    //Gallery  Subtitle
    $cmb_structure->add_field( 
        array(
        'name' => __('Gallery title (minor)', 'lepartiecomemoracoes-wp-muplugin'),
        'desc' => '',
        'type' => 'text',
        'id'   => $prefix.$section.'gallery_subtitle',
        )
    );

    //Gallery  Title
    $cmb_structure->add_field( 
        array(
        'name' => __('Gallery title (larger)', 'lepartiecomemoracoes-wp-muplugin'),
        'desc' => '',
        'type' => 'text',
        'id'   => $prefix.$section.'gallery_title',
        )
    );
    
    //Gallery Background image
    $cmb_structure->add_field(
        array(
            'name'    => __('Gallery Background Image', 'lepartiecomemoracoes-wp-muplugin'),
            'desc'    => '',
            'id'      => $prefix.$section.'gallery-bg-image',
            'type'    => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'lepartiecomemoracoes-wp-muplugin'), // Change upload button text. Default: "Add or Upload File"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                ),
            ),
            'preview_size' => array(320, 180) //'large', // Image size to use when previewing in the admin.
        )
    );

    //Gallery Group
    $structure_category_id = $cmb_structure->add_field( 
        array(
        'id'          => $prefix.$section.'category',
        'type'        => 'group',
        'description' => '', 
        // 'repeatable'  => false, // use false if you want non-repeatable group
        'options'     => array(
            'group_title'   => __('Category {#}', 'lepartiecomemoracoes-wp-muplugin'), // since version 1.1.4, {#} gets replaced by row number
            'add_button'    => __('Add Another Entry', 'lepartiecomemoracoes-wp-muplugin'),
            'remove_button' => __('Remove Entry', 'lepartiecomemoracoes-wp-muplugin'),
            'sortable'      => true, // beta
            // 'closed'     => true, // true to have the groups closed by default
        ),
        )
    );
        
    //Category Title
    $cmb_structure->add_group_field( 
        $structure_category_id, array(
        'name' => __('Title', 'lepartiecomemoracoes-wp-muplugin'),
        'id'   => 'title',
        'type' => 'text',
        ) 
    );

    //Structure Gallery
    $cmb_structure->add_group_field( 
        $structure_category_id, array(
            'name' => __('Images', 'lepartiecomemoracoes-wp-muplugin'),
            'id'   => 'images',
            'type' => 'file_list',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text' => array(
                'add_upload_files_text' => __('Add Images', 'lepartiecomemoracoes-wp-muplugin'), // default: "Add or Upload Files"
                'remove_image_text' => __('Remove Image', 'lepartiecomemoracoes-wp-muplugin'), // default: "Remove Image"
                'file_text' => __('File', 'lepartiecomemoracoes-wp-muplugin'), // default: "File:"
                'file_download_text' => __('Download', 'lepartiecomemoracoes-wp-muplugin'), // default: "Download"
                'remove_text' => __('Remove', 'lepartiecomemoracoes-wp-muplugin'),
                'use_text' => __('Remove', 'lepartiecomemoracoes-wp-muplugin'), // default: "Remove"
                'upload_file'  => __('Use this file', 'lepartiecomemoracoes-wp-muplugin'),
                'upload_files' => __('Use these files', 'lepartiecomemoracoes-wp-muplugin'),
                'remove_image' => __('Remove Image', 'lepartiecomemoracoes-wp-muplugin'),
                'remove_file'  => __('Remove', 'lepartiecomemoracoes-wp-muplugin'),
                'file'         => __('File:', 'lepartiecomemoracoes-wp-muplugin'),
                'download'     => __('Download', 'lepartiecomemoracoes-wp-muplugin'),
                'check_toggle' => __('Select / Deselect All', 'lepartiecomemoracoes-wp-muplugin'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
            ),
            'preview_size' => array(160, 90) //'large', // Image size to use when previewing in the admin.
        )
    );
    
    /********************************************************************
     * Photos section
     *******************************************************************/
    $section = 'photos_';
    $cmb_photos = new_cmb2_box(
        array(
            'id'            => 'photos_id',
            'title'         => __('Photos', 'lepartiecomemoracoes-wp-muplugin'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'front-page', 'value' => '' ), // function should return a bool value
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    //Photos background image
    $cmb_photos->add_field(
        array(
            'name'    => __('Background Image', 'lepartiecomemoracoes-wp-muplugin'),
            'desc'    => '',
            'id'      => $prefix.$section.'bg-image',
            'type'    => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'lepartiecomemoracoes-wp-muplugin'), // Change upload button text. Default: "Add or Upload File"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                ),
            ),
            'preview_size' => array(320, 180) //'large', // Image size to use when previewing in the admin.
        )
    );

    //Photos Gallery
    $cmb_photos->add_field(
        array(
            'name' => __('Gallery', 'lepartiecomemoracoes-wp-muplugin'),
            'id'   => $prefix.$section.'gallery',
            'type' => 'file_list',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text' => array(
                'add_upload_files_text' => __('Add Images', 'lepartiecomemoracoes-wp-muplugin'), // default: "Add or Upload Files"
                'remove_image_text' => __('Remove Image', 'lepartiecomemoracoes-wp-muplugin'), // default: "Remove Image"
                'file_text' => __('File', 'lepartiecomemoracoes-wp-muplugin'), // default: "File:"
                'file_download_text' => __('Download', 'lepartiecomemoracoes-wp-muplugin'), // default: "Download"
                'remove_text' => __('Remove', 'lepartiecomemoracoes-wp-muplugin'),
                'use_text' => __('Remove', 'lepartiecomemoracoes-wp-muplugin'), // default: "Remove"
                'upload_file'  => __('Use this file', 'lepartiecomemoracoes-wp-muplugin'),
                'upload_files' => __('Use these files', 'lepartiecomemoracoes-wp-muplugin'),
                'remove_image' => __('Remove Image', 'lepartiecomemoracoes-wp-muplugin'),
                'remove_file'  => __('Remove', 'lepartiecomemoracoes-wp-muplugin'),
                'file'         => __('File:', 'lepartiecomemoracoes-wp-muplugin'),
                'download'     => __('Download', 'lepartiecomemoracoes-wp-muplugin'),
                'check_toggle' => __('Select / Deselect All', 'lepartiecomemoracoes-wp-muplugin'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
            ),
            'preview_size' => array(160, 90) //'large', // Image size to use when previewing in the admin.
        )
    );

    /*
    //Photos Gallery Group
    $photos_id = $cmb_photos->add_field(
        array(
            'id'          => $prefix.$section.'gallery',
            'type'        => 'group',
            'description' => '',
            'repeatable'  => true, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Photo {#}', 'lepartiecomemoracoes-wp-muplugin'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Entry', 'lepartiecomemoracoes-wp-muplugin'),
                'remove_button' => __('Remove Entry', 'lepartiecomemoracoes-wp-muplugin'),
                'sortable'      => true, // beta
                // 'closed'     => true, // true to have the groups closed by default
            ),
        )
    );

     //Photos image
    $cmb_photos->add_group_field(
        $photos_id,
        array(
            'name'    => __('Image', 'lepartiecomemoracoes-wp-muplugin'),
            'desc'    => '',
            'id'      => 'image',
            'type'    => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'lepartiecomemoracoes-wp-muplugin'), // Change upload button text. Default: "Add or Upload File"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                ),
            ),
            'preview_size' => array(320, 180) //'large', // Image size to use when previewing in the admin.
        )
    );
    
    //Thumbnail aspect ratio
    $cmb_photos->add_group_field( 
        $photos_id, 
        array(
            'name'    => __('Thumbnail', 'lepartiecomemoracoes-wp-muplugin'),
            'id'      => 'thumbnail',
            'type'    => 'radio_inline',
            'options' => array(
                'square' => __('Square', 'lepartiecomemoracoes-wp-muplugin'),
                'landscape' => __('Landscape', 'lepartiecomemoracoes-wp-muplugin'),
                'portraid' => __('Portraid', 'lepartiecomemoracoes-wp-muplugin'),
            ),
            'default' => 'square',
        ) 
    );
    */

    /********************************************************************
     * Contact section
     *******************************************************************/
    $section = 'contact_';
    $cmb_contact = new_cmb2_box(
        array(
            'id'            => 'contact_id',
            'title'         => __('Contact', 'lepartiecomemoracoes-wp-muplugin'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'front-page', 'value' => '' ), // function should return a bool value
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    // Contact background image
    $cmb_contact->add_field(
        array(
            'name'    => __('Background Image', 'lepartiecomemoracoes-wp-muplugin'),
            'desc'    => '',
            'id'      => $prefix.$section.'bg-image',
            'type'    => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'lepartiecomemoracoes-wp-muplugin'), // Change upload button text. Default: "Add or Upload File"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                ),
            ),
            'preview_size' => array(320, 180) //'large', // Image size to use when previewing in the admin.
        )
    );

    //Contact Address
    $cmb_contact->add_field( 
        array(
            'name' => __('Address', 'lepartiecomemoracoes-wp-muplugin'),
            'id'   => $prefix.$section.'address',
            'type' => 'text',
        ) 
    );

    //Contact Phone
    $cmb_contact->add_field( 
        array(
            'name' => __('Phone', 'lepartiecomemoracoes-wp-muplugin'),
            'id'   => $prefix.$section.'phone',
            'type' => 'text',
        ) 
    );

    //Contact Phone
    $cmb_contact->add_field( 
        array(
            'name' => __('Cell Phone', 'lepartiecomemoracoes-wp-muplugin'),
            'id'   => $prefix.$section.'cell-phone',
            'type' => 'text',
        ) 
    );

}

/*
  function LepartieComemoracoes_Show_On_page($cmb) 
 {
    $slug = get_page_template_slug($cmb->object_id);
    //echo $slug;
    return in_array($slug, array('views/front-page.blade.php'));
}
*/

add_action('cmb2_admin_init', 'cmb_lepartiecomemoracoes_page');

//******************************************************************************************************************************************************/
